# gtk-recent.vim - a gnome logger for Vim


## Behaviour

If you modify a file with vim and want to upload it on a web interface, you wish you could find this file in Firefox's "recent files" category. Well now you can : with this plugin, each opened file will be added to Gnome's recent list (and to Zeitgeist as a consequence, see [Gnome's wiki](https://wiki.gnome.org/Attic/GtkRecentManagerAndZeitgeist#Usage_in_applications)).

## Limitations

This thing adds quite a huge latency to vim startup/shutdown. I made a C
version that you can find [here](git.relatif.moi/lafleur/flagRecent).

## Installation

If you're using Arch linux, there's a PKGBUILD : https://aur.archlinux.org/packages/vim-gtk-recent-git .

Drop this file in a vim plugin folder ($HOME/.vim/plugin,/usr/share/vim/vimfiles/plugin, ...). Vim should be compiled with python enabled.
You will need to have python-gobject installed, and vim or neovim with python support.

## History

Derived from :
https://bougui505.github.io/2016/04/20/adding_vim_data-source_to_zeitgeist.html
Improved to use python 3, and to use [GtkRecentManager](https://bstern.org/gtk+-2.24/GtkRecentManager.html).

## License

This script is licensed under the GPLv3 license.
