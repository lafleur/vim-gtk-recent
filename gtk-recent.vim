" gtk-recent.vim - a gnome logger for Vim
" Author : la Fleur <lafleur@boum.org>
" Installation : drop this file in a vim plugin folder ($HOME/.vim/plugin,/usr/share/vim/vim72/plugin, ...). Vim should be compiled with python enabled.
" Each opened file will be added to gnome's recent list and zeitgeist.
" Derived from :
" https://bougui505.github.io/2016/04/20/adding_vim_data-source_to_zeitgeist.html

if v:version < 703
    finish
endif

function! GtkRecentLog(filename,use_id)
python3 << endpython
filename = vim.eval("a:filename")
manager = Gtk.RecentManager()
recent_data = Gtk.RecentData()

recent_data.app_name = "vi"
recent_data.mime_type = "text/plain"
recent_data.app_exec = "vi"
manager.add_full("file://" + filename, recent_data)
endpython
endfunction

python3 << endpython
import os
import time
import vim
from gi import require_version
require_version('Gtk', '3.0')
from gi.repository import Gtk
endpython

if match(system('whoami'), 'root') == -1
    augroup gtkRecent
    au!
    au BufRead * call GtkRecentLog (expand("%:p"), "read")
    au BufNewFile * call GtkRecentLog (expand("%:p"), "new")
    au BufWrite * call GtkRecentLog (expand("%:p"), "write")
    augroup END
endif
